<?php include("includes/header.php"); ?>

<?php

$page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;

$items_per_page =  12;

$items_total_count = Photo::countAll();




//$photos = Photo::getAll();

$pagination = new Pagination($page, $items_per_page, $items_total_count);

$sql = "SELECT * FROM photos LIMIT {$items_per_page} OFFSET {$pagination->offset()}";

$photos = Photo::findByQuery($sql);


?>

<div class="row">

    <!-- Blog Entries Column -->
<!--    <div class="col-md-8">-->

        <div class="thumbnails row">

            <?php  foreach ($photos as $photo):  ?>

                <div class="col-xs-6 col-md-3 photo-section">
                    <a class="thumbnail" href="photo.php?id=<?php echo $photo->id; ?>">
                        <img class="homepage-photo img-responsive " src="admin/<?php echo $photo->picturePath(); ?>" title="<?php echo $photo->title ?>" alt="">
                    </a>
                </div>

            <?php endforeach; ?>

        </div>

<!--    Pagination-->
    <div class="row">
        <ul class="pager">
            <?php if ($pagination->pageTotal() > 1) : ?>
                <?php if ($pagination->hasPrevious()) : ?>
                    <li class="previous"><a href="index.php?page=<?php echo $pagination->previous(); ?>">Previous Page</a></li>
                <?php endif; ?>

                    <?php for ($i = 1; $i <= $pagination->pageTotal(); $i++) : ?>
                        <?php if ($i == $pagination->current_page) : ?>
                            <li class="active"><a class="active_link" href="index.php?page=<?php echo $i; ?>"><?php echo $i ?></a></li>
                        <?php else : ?>
                            <li class=""><a href="index.php?page=<?php echo $i; ?>"><?php echo $i ?></a></li>
                        <?php endif; ?>
                    <?php endfor; ?>

                <?php if ($pagination->hasNext()) : ?>
                    <li class="next"><a href="index.php?page=<?php echo $pagination->next(); ?>" class="">Next Page</a></li>
                <?php endif; ?>
            <?php endif; ?>
        </ul>
    </div>
<!--    </div>-->
</div>


<?php include("includes/footer.php"); ?>
