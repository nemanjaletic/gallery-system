<?php
require_once ("admin/includes/init.php");

if (empty($_GET['id'])) {
    redirect('index.php');
}
$photo = Photo::getById($_GET['id']);


if (isset($_POST['submit'])) {

    $author = trim($_POST['author']);
    $body = trim($_POST['body']);
    $date = date('d.m.y H:i');

    $new_comment = Comment::createComment($photo->id, $author, $body, $date);

    if ($new_comment && $new_comment->save()) {
        redirect("photo.php?id={$photo->id}");
    } else {
        $session->message("There was some problems saving.");
    }
} else {
    $author = "";
    $body = "";
}

$comments = Comment::getComments($photo->id);

?>


<?php include("includes/header.php"); ?>

<!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8 row">

                <!-- Blog Post -->

                <!-- Title -->
                <h1><?php echo $photo->title ?></h1>

                <p class="lead">
                <?php if ($photo->user_id) : ?>
                    <?php $user = User::getById($photo->user_id) ?>
                        by <a href="#"><?php echo $user->username ?></a>
                    <?php else: ?>
                        by Anonymous
                    </p>
                <?php endif; ?>

                <hr>

                <!-- Date/Time -->
                <?php $date = substr($photo->date, 0,10); ?>
                <?php $time = substr($photo->date, 11,5); ?>
                <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $date ?> at <?php echo $time ?></p>

                <hr>

                <!-- Preview Image -->
                <img class="img-responsive" src="<?php echo "admin/".$photo->picturePath() ?>" alt="">

                <hr>

                <!-- Post Content -->
                <p class="lead"><?php echo $photo->description ?></p>

                <hr>

                <!-- Blog Comments -->

                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form role="form" method="post">
                        <div class="form-group">
                            <label for="author">Your Name</label>
                            <input type="text" class="form-control" name="author" id="author">
                        </div>
                        <div class="form-group">
                            <label for="body">Comment</label>
                            <textarea class="form-control" name="body" id="body" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                    </form>
                </div>

                <hr>

                <?php foreach ($comments as $comment) : ?>
                    <div class="media">
                        <a class="pull-left" href="#">
                            <img class="media-object" src="http://placehold.it/64x64" alt="">
                        </a>
                        <div class="media-body">
                            <?php $date = substr($comment->date, 0,10); ?>
                            <?php $time = substr($comment->date, 11,5); ?>
                            <h4 class="media-heading"><?php echo $comment->author; ?>
                                <small><?php echo $date ?> at <?php echo $time ?></small>
                            </h4>
                            <?php echo $comment->body; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">


                <?php include("includes/sidebar.php"); ?>



            </div>
            <!-- /.row -->

            <?php include("includes/footer.php"); ?>
