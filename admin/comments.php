<?php include("includes/header.php"); ?>
<?php if (!$session->isSignedIn()) : ?>
    <?php redirect("login.php"); ?>
<?php endif; ?>

<?php
$page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;

$items_per_page =  12;

$items_total_count = Comment::countAll();




//$photos = Photo::getAll();

$pagination = new Pagination($page, $items_per_page, $items_total_count);

$sql = "SELECT * FROM comments ORDER BY id DESC LIMIT {$items_per_page} OFFSET {$pagination->offset()}";

$comments = Comment::findByQuery($sql);

?>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

        <!-- Navigation -->
        <?php include "includes/navigation.php" ?>

        <?php include "includes/sidebar.php" ?>
    </nav>
    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <p class="bg-info"><?php echo $message; ?></p>
                    <h1 class="page-header">
                        Comments
                        <small>Manage Comments</small>
                    </h1>


                    <div class="col-md-12">
                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Author</th>
                                <th>Body</th>
                                <th>Photo ID</th>
                                <th colspan="3" class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($comments as $comment) : ?>
                                <tr>
                                    <!--                                <td><img src="images/--><?php //echo $comment->filename; ?><!--'></td>-->
<!--                                    <td><img class="img-responsive img-rounded" src="--><?php //echo $comment->picturePath(); ?><!--"></td>-->
                                    <td><?php echo $comment->id; ?></td>
                                    <td><?php echo $comment->author; ?></td>
                                    <td><?php echo $comment->body; ?></td>
                                    <td><a href="comments_photo.php?id=<?php echo $comment->photo_id ?>"><?php echo $comment->photo_id ?></a></td>
                                    <td><a class="delete_link" href="delete_comment.php?id=<?php echo $comment->id ?>">Delete</a></td>
                                    <td><a href="edit_comment.php?id=<?php echo $comment->id ?>">Edit</a></td>
                                    <td><a href="../photo.php?id=<?php echo $comment->photo_id ?>">View</a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>

                        <!--    Pagination-->
                        <div class="row">
                            <ul class="pager">
                                <?php if ($pagination->pageTotal() > 1) : ?>
                                    <?php if ($pagination->hasPrevious()) : ?>
                                        <li class="previous"><a href="comments.php?page=<?php echo $pagination->previous(); ?>">Previous Page</a></li>
                                    <?php endif; ?>

                                    <?php for ($i = 1; $i <= $pagination->pageTotal(); $i++) : ?>
                                        <?php if ($i == $pagination->current_page) : ?>
                                            <li class="active"><a class="active_link" href="comments.php?page=<?php echo $i; ?>"><?php echo $i ?></a></li>
                                        <?php else : ?>
                                            <li class=""><a href="comments.php?page=<?php echo $i; ?>"><?php echo $i ?></a></li>
                                        <?php endif; ?>
                                    <?php endfor; ?>

                                    <?php if ($pagination->hasNext()) : ?>
                                        <li class="next"><a href="comments.php?page=<?php echo $pagination->next(); ?>" class="">Next Page</a></li>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </ul>
                        </div>

                    </div>

                </div>
            </div>
            <!-- /.row -->

        </div>

    </div>
    <!-- /#page-wrapper -->

<?php include("includes/footer.php"); ?>