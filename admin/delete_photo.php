<?php include "includes/init.php" ?>

<?php if (!$session->isSignedIn()) : ?>
    <?php redirect("login.php"); ?>
<?php endif; ?>

<?php
if (empty($_GET['id'])) {
    redirect("photos.php");
}

$photo = Photo::getById($_GET['id']);

if ($photo) {

    Comment::deleteCommentsByPhotoId($_GET['id']);
    $photo->deletePhoto();
    redirect("photos.php");
    $session->message("The {$photo->filename} has been Deleted!");
} else {
    redirect("photos.php");
}

?>
