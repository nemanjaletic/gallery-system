<?php include("includes/header.php"); ?>
<?php if (!$session->isSignedIn()) : ?>
    <?php redirect("login.php"); ?>
<?php endif; ?>

<?php
if (empty($_GET['id'])) {
    redirect("users.php");
} else {
    $userById = User::getById($_GET['id']);

    if (isset($_POST['update'])) {
        if ($userById) {
            $userById->first_name = $_POST['first_name'];
            $userById->last_name = $_POST['last_name'];
            $userById->username = $_POST['username'];
            $userById->email = $_POST['email'];
            $userById->password = $_POST['password'];

            if (empty($_FILES['user_image'])) {
                $userById->save();
                redirect("users.php");
                $session->message("The User {$userById->username} has been Updated!");
            } else {

                $userById->setFile($_FILES['user_image']);
                $userById->saveUser();
                $userById->save();

//                redirect("edit_user.php?id={$userById->id}");
                redirect("users.php");
                $session->message("The User {$userById->username} has been Updated!");

            }

        }
    }
}


?>


    <?php include("includes/photo_library_modal.php"); ?>


    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

        <!-- Navigation -->
        <?php include "includes/navigation.php" ?>

        <?php include "includes/sidebar.php" ?>
    </nav>
    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        User
                        <small>Edit User</small>
                    </h1>
                    <div class="col-md-6 user_image_box">
                        <a href="" class="" data-toggle="modal" data-target="#photo-library">
                            <img class="img-responsive img-rounded" src="<?php echo $userById->userImage(); ?>">
                        </a>
                    </div>
                    <form action="" method="post"  enctype="multipart/form-data">
                        <div class="col-md-6">

                            <div class="form-group">

                                <input type="file" name="user_image">

                            </div>
                            <div class="form-group">
                                <label for="">First Name</label>
                                <input type="text" class="form-control" name="first_name" value="<?php echo $userById->first_name ?>">
                            </div>

                            <div class="form-group">
                                <label for="">Last Name</label>
                                <input type="text" class="form-control" name="last_name" value="<?php echo $userById->last_name ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="text" class="form-control" name="username" value="<?php echo $userById->username ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" class="form-control" name="email" value="<?php echo $userById->email ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Password</label>
                                <input type="password" class="form-control" name="password" value="<?php echo $userById->password ?>">
                            </div>
                            <div class="info-box-update pull-right ">
                                <input type="submit" name="update" value="Update" class="btn btn-primary btn-lg ">
                            </div>
                            <div class="info-box-delete pull-left">
                                <a id="user-id" href="delete_user.php?id=<?php echo $userById->id; ?>" class="btn btn-danger btn-lg ">Delete</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /#page-wrapper -->

<?php include("includes/footer.php"); ?>