$(document).ready(function(){

    var user_href;
    var user_href_splitted;
    var user_id;

    var image_src;
    var image_href_splitted;
    var image_name;

    var photo_id;



    // Get class modal_thumbnails, and on click...
    $(".modal_thumbnails").click(function(){
        // Set button with id set_user_image to enabled
        $("#set_user_image").prop('disabled', false);


        user_href = $("#user-id").prop('href');
        user_href_splitted = user_href.split("=");
        // Length = 2 -1
        user_id = user_href_splitted[user_href_splitted.length -1];

        image_src = $(this).prop("src");
        image_href_splitted = image_src.split("/");

        image_name = image_href_splitted[image_href_splitted.length -1];

        photo_id = $(this).attr("data");

        $.ajax({
            url: "includes/ajax_code.php",
            data:{photo_id: photo_id},
            type: "POST",
            success: function(data){
                if(!data.error) {
                    $("#modal_sidebar").html(data);
                }
            }

        });

    });

    $("#set_user_image").click(function(){

        // Get data from ajax_code.php
        $.ajax({
            url: "includes/ajax_code.php",
            data:{image_name: image_name, user_id: user_id},
            type: "POST",
            success: function(data){
                if(!data.error) {
                    $(".user_image_box a img").prop('src', data);
                }
            }

        });

    });


    // Edit Photo sidebar

    // select class
    $(".info-box-header").click(function(){
        // add slide on class
        $(".inside").slideToggle("fast");
        // find toggle id and selecre toggles class
        $("#toggle").toggleClass("glyphicon-menu-down glyphicon-menu-up");
    });

    // WYSIWYG editor
    tinymce.init({selector:'textarea'});


    // Delete

    $(".delete_link").click(function(){
        if (!confirm("Are you sure you want to Delete this item?")){
            return false;
        }
    });


});
