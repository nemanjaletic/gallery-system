<?php include("includes/header.php"); ?>
<?php if (!$session->isSignedIn()) : ?>
    <?php redirect("login.php"); ?>
<?php endif; ?>

<?php
if (empty($_GET['id'])) {
    redirect('comments.php');
} else {
    $comments = Comment::getComments($_GET['id']);
}
?>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

        <!-- Navigation -->
        <?php include "includes/navigation.php" ?>

        <?php include "includes/sidebar.php" ?>
    </nav>
    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Comments
                        <small>Manage Comments</small>
                    </h1>

                    <div class="col-md-12">
                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Author</th>
                                <th>Body</th>
                                <th>Photo ID</th>
                                <th colspan="3" class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($comments as $comment) : ?>
                                <tr>
                                    <!--                                <td><img src="images/--><?php //echo $comment->filename; ?><!--'></td>-->
                                    <!--                                    <td><img class="img-responsive img-rounded" src="--><?php //echo $comment->picturePath(); ?><!--"></td>-->
                                    <td><?php echo $comment->id; ?></td>
                                    <td><?php echo $comment->author; ?></td>
                                    <td><?php echo $comment->body; ?></td>
                                    <td><a href="edit_photo.php?id=<?php echo $comment->photo_id ?>"><?php echo $comment->photo_id ?></a></td>
                                    <td><a href="delete_comment.php?id=<?php echo $comment->id ?>">Delete</a></td>
                                    <td><a href="edit_comment.php?id=<?php echo $comment->id ?>">Edit</a></td>
                                    <td><a href="view_comment.php?id=<?php echo $comment->id ?>">View</a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!-- /.row -->

        </div>

    </div>
    <!-- /#page-wrapper -->

<?php include("includes/footer.php"); ?>