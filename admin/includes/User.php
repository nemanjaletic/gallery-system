<?php

class User extends DbObject
{

    protected static $db_table = "users";

    protected static $db_table_fields = array('first_name', 'last_name', 'email', 'username', 'password', 'user_image');

    public $id;
    public $first_name;
    public $last_name;
    public $email;
    public $username;
    public $password;
    public $user_image;

    public $tmp_path;

    public $upload_directory = "images";
    public $image_placeholder = "http://placehold.it/150x150&text=Profile Picture";

    public $errors = array();
    public $upload_errors_array = array(
        UPLOAD_ERR_OK => "There is no error",
        UPLOAD_ERR_INI_SIZE => "The uploaded file exceeds the upload_max_filesize directive in php.ini",
        UPLOAD_ERR_FORM_SIZE => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
        UPLOAD_ERR_PARTIAL => "The uploaded file was only partially uploaded.",
        UPLOAD_ERR_NO_FILE => "No file was uploaded.",
        UPLOAD_ERR_NO_TMP_DIR => "Missing a temporary folder.",
        UPLOAD_ERR_CANT_WRITE => "Failed to write file to disk.",
        UPLOAD_ERR_EXTENSION => "A PHP extension stopped the file upload."
    );


    /**
     * Return User img if exists or return a placeholder
     * @return string
     */
    public function userImage() {
        return empty($this->user_image) ? $this->image_placeholder : $this->upload_directory.DS.$this->user_image;
    }

    /**
     * Verify User
     * @param $username
     * @param $password
     * @return bool|mixed
     */
    public static function verifyUser($username, $password) {

        global $database;

        $username = $database->escape_string($username);
        $password = $database->escape_string($password);

        $user = static::findByQuery("SELECT * FROM " . static::$db_table . " WHERE username = '{$username}' AND password = '{$password}' LIMIT 1");

        return !empty($user) ? array_shift($user) : false;

    }

    // This is passing $_FILES['uploaded_file'] as an argument

    /**
     * Set File
     * @param $file
     * @return bool
     */
    public function setFile($file)
    {
        if (empty($file) || !$file || !is_array($file)) {
            $this->errors[] = "There was no file uploaded here";
            return false;

        } elseif ($file['error'] != 0) {

            $this->errors[] = $this->upload_errors_array[$file['error']];
            return false;

        } else {

            $this->user_image = basename($file['name']);
            $this->tmp_path = $file['tmp_name'];
            $this->type = $file['type'];
            $this->size = $file['size'];

        }
    }

    /**
     * Upload Photo
     * @return bool
     */
    public function uploadPhoto() {

        if(!empty($this->errors)) {

            return false;

        }

        if(empty($this->user_image) || empty($this->tmp_path)){
            $this->errors[] = "the file was not available";
            return false;
        }

        $target_path = SITE_ROOT . DS . $this->user_image;


        if(file_exists($target_path)) {
            $this->errors[] = "The file {$this->user_image} already exists";
            return false;
        }

        if(move_uploaded_file($this->tmp_path, $target_path)) {
            unset($this->tmp_path);
            return true;

        } else {

            $this->errors[] = "the file directory probably does not have permission";
            return false;

        }
    }

    /**
     * Save User
     * @return bool
     */
    public function saveUser()
    {

        if ($this->id) {

            $this->update();
            $target_path = SITE_ROOT . DS . $this->user_image;

            move_uploaded_file($this->tmp_path, $target_path);
        } else {

            if (!empty($this->errors)) {
                return false;
            }

            if (empty($this->user_image) || empty($this->tmp_path)) {
                $this->errors[] = "The file was not available";
                return false;
            }

            // TODO: FIX THIS!!!
//            $target_path = SITE_ROOT . DS . 'images' . DS . $this->upload_directory . DS . $this->user_image;

            $target_path = SITE_ROOT . DS . $this->user_image;
//            $target_path = "images/".$this->user_image;

            if (file_exists($target_path)) {
                $this->errors[] = "The file {$this->user_image} already exists";
                return false;

            }

            if (move_uploaded_file($this->tmp_path, $target_path)) {

                    unset($this->tmp_path);
                    return true;

            } else {

                $this->errors[] = "The file directory probably does not have permission";
                return false;

            }
        }

    }

    /**
     * Return picture path
     * @return string
     */
    public function picturePath() {
        return $this->upload_directory.DS.$this->user_image;
    }

    /**
     * Delete User
     * @return bool
     */
    public function deleteUser() {
        if ($this->delete()) {

            return unlink($this->picturePath()) ? true : false;
        } else {
            return false;
        }

    }

    /**
     * @param $user_image
     * @param $user_id
     */
    public function ajaxSaveUserImage($user_image, $user_id) {
        global $database;

        $user_image = $database->escape_string($user_image);
        $user_id = $database->escape_string($user_id);

        $this->user_image = $user_image;
        $this->id = $user_id;

        $sql = "UPDATE " . self::$db_table . " SET user_image = '{$this->user_image}' WHERE id = {$this->id}";
        $update_image = $database->query($sql);

        echo $this->userImage();
    }

}
