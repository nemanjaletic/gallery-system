<?php
/**
 * Created by PhpStorm.
 * User: nemanjaletic
 * Date: 13.8.20.
 * Time: 09.08
 */

class Comment extends DbObject
{

    protected static $db_table = "comments";
    protected static $db_table_fields = array('author', 'body', 'photo_id', 'date');

    public $id;
    public $author;
    public $body;
    public $photo_id;
    public $date;

    public static function createComment($photo_id, $author, $body, $date) {
        if (!empty($photo_id) && !empty($author) && !empty($body)) {
            $comment = new Comment();

            $comment->photo_id = $photo_id;
            $comment->author = $author;
            $comment->body = $body;
            $comment->date = $date;

//            $comment->save();

            return $comment;
        } else {
            die('no way man');
            return false;
        }
    }

    public static function getComments($photo_id = 0) {
        global $database;
        $sql = "SELECT * FROM " . self::$db_table . " WHERE photo_id =" . $database->escape_string($photo_id) . " ORDER BY photo_id ASC";

        return self::findByQuery($sql);
    }

    public static function deleteCommentsByPhotoId($photo_id) {
        global $database;
        $sql = "DELETE FROM comments WHERE photo_id = " . $database->escape_string($photo_id);

        $database->query($sql);

        return (mysqli_affected_rows($database->connection) == 1) ? true : false;

    }

}