<?php

function classAutoLoader($class) {

    $path = "includes/{$class}.php";

    file_exists($path) ? require_once $path : die ("This file named {$class} wasn't found.");
}

//__autolad is deprecated
//spl_autoload_register(function($class) {
//
////    $class = strtolower($class);
//
//    $path = "includes/{$class}.php";
//
//    if (file_exists($path)) {
//        require_once($path);
//    } else {
//        die ("This file name {$class}.php wasn't found.");
//    }
//});
//


spl_autoload_register('classAutoloader');

function redirect($location) {
    header("Location: {$location}");
}

function escape($string) {

    global $connection;

    return mysqli_real_escape_string($connection, trim($string));
}