<?php
/**
 * Created by PhpStorm.
 * User: nemanjaletic
 * Date: 7.8.20.
 * Time: 11.45
 */

class DbObject
{
    /**
     * Get all field from static array properties and return array of db table fields
     * @return array
     */

    protected function properties() {
        // Return all properties (id, first_name, last_name, username, email, password)

        $properties = array();

        foreach (static::$db_table_fields as $db_field) {
            if (property_exists($this, $db_field)) {
                $properties[$db_field] = $this->$db_field;
            }
        }

        return $properties;
    }

    /**
     * Create empty array, get all properties from properties() function, go through the loop and escape string
     * @return array
     */
    protected function clean_properties() {
        global $database;

        $clean_properties = array();

        // Get all properties in and escape them all
        foreach ($this->properties() as $key => $value) {
            $clean_properties[$key] = $database->escape_string($value);
        }

        return $clean_properties;
    }

    /**
     * Find requested query function
     * @param $sql
     * @return array|User
     */
    public static function findByQuery($sql) {

        global $database;

        $result_set = $database->query($sql);

        $the_object_array = array();

        while ($row = mysqli_fetch_array($result_set)) {
            // $user_id = $row['id];
            $the_object_array[] = static::instantiate($row);
        }

        return $the_object_array;
    }

    /**
     * @param $the_record
     * @return mixed
     */
    public static function instantiate($the_record) {

        $calling_class = get_called_class();
        // Create new object of User  class
        $the_object = new $calling_class;


//        $the_object->username = $found_user['username'];
//        $the_object->password = $found_user['password'];
        //              $found_user => ['username']
        foreach ($the_record as $the_attribute => $value) {
            if ($the_object->hasTheAttribute($the_attribute)) {
                $the_object->$the_attribute = $value;
            }
        }
        return $the_object;
    }

    /**
     * Is object attribute
     * @param $the_attribute
     * @return bool
     */
    public function hasTheAttribute($the_attribute) {

        $object_properties = get_object_vars($this);

        // If the_attribute exists in object_properties, return true
        return array_key_exists($the_attribute, $object_properties);

    }

    /**
     * If you have a table ID, update it, otherwise create a new row.
     * @return bool
     */
    public function save() {
        return isset($this->id) ? $this->update() : $this->create();
    }

    /**
     * Get all from database table
     */
    public static function getAll() {
        return static::findByQuery("SELECT * FROM ". static::$db_table . " ");
    }

    /**
     * Function for getting by table id
     * @param $id
     * @return bool|mixed
     */
    public static function getById($id) {
        $the_result_array = static::findByQuery("SELECT * FROM " . static::$db_table . " WHERE id = {$id} LIMIT 1");

        return !empty($the_result_array) ? array_shift($the_result_array) : false;
    }


    /**
     * Create new row
     * @return bool
     */
    public function create() {

        global $database;

        $properties = $this->clean_properties();

        $sql = "INSERT INTO " . static::$db_table . "(" . implode(",", array_keys($properties)) . ") ";
        $sql .= "VALUES ('" . implode("','", array_values($properties)) . "')"  ;

        if ($database->query($sql)) {
            $this->id = $database->the_insert_id();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update Row
     * @return bool
     */
    public function update() {
        global $database;

        $properties = $this->clean_properties();

        $properties_pairs = array();

        foreach ($properties as $key => $value) {
            $properties_pairs[] = "{$key}='{$value}' ";
        }

        $sql = "UPDATE " . static::$db_table ." SET ";
//        $sql .= "first_name = '" . $database->escape_string($this->first_name) . "', ";
        $sql .= implode(", ", $properties_pairs);
        $sql .= " WHERE id = " .$this->id;
        $database->query($sql);

        return (mysqli_affected_rows($database->connection) == 1) ? true : false;
    }

    /**
     * Delete from table
     * @return bool
     */
    public function delete() {
        global $database;

        $sql = "DELETE FROM " . static::$db_table . " WHERE id = " . $database->escape_string($this->id) . " LIMIT 1";

        $database->query($sql);

        return (mysqli_affected_rows($database->connection) == 1) ? true : false;

    }

    /**
     * Count All rows from table
     * @return mixed
     */
    public static function countAll() {
        global $database;

        $sql = "SELECT COUNT(*) FROM " . static::$db_table;
        $result_set = $database->query($sql);

        $row = mysqli_fetch_array($result_set);

        return array_shift($row);
    }


}