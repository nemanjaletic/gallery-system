<?php
/**
 * Created by PhpStorm.
 * User: nemanjaletic
 * Date: 7.8.20.
 * Time: 14.13
 */

class Photo extends DbObject
{


    protected static $db_table = "photos";
    protected static $db_table_fields = array('title', 'description', 'filename', 'type', 'size', 'caption', 'alternate_text', 'date', 'user_id');
    public $id;
    public $title;
    public $caption;
    public $description;
    public $alternate_text;
    public $filename;
    public $type;
    public $size;
    public $date;
    public $user_id;


    public $tmp_path;
    public $upload_directory = "images";

    public $errors = array();
    public $upload_errors_array = array(
        UPLOAD_ERR_OK => "There is no error",
        UPLOAD_ERR_INI_SIZE => "The uploaded file exceeds the upload_max_filesize directive in php.ini",
        UPLOAD_ERR_FORM_SIZE => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
        UPLOAD_ERR_PARTIAL => "The uploaded file was only partially uploaded.",
        UPLOAD_ERR_NO_FILE => "No file was uploaded.",
        UPLOAD_ERR_NO_TMP_DIR => "Missing a temporary folder.",
        UPLOAD_ERR_CANT_WRITE => "Failed to write file to disk.",
        UPLOAD_ERR_EXTENSION => "A PHP extension stopped the file upload."
    );

    // This is passing $_FILES['uploaded_file'] as an argument

    /**
     * @param $file
     * @return bool
     */
    public function setFile($file)
    {
        if (empty($file) || !$file || !is_array($file)) {
            $this->errors[] = "There was no file uploaded here";
            return false;

        } elseif ($file['error'] != 0) {

            $this->errors[] = $this->upload_errors_array[$file['error']];
            return false;

        } else {

            $this->filename = basename($file['name']);
            $this->tmp_path = $file['tmp_name'];
            $this->type = $file['type'];
            $this->size = $file['size'];

        }
    }

    /**
     * Save the photo if it doesn't exist
     * @return bool
     */
    public function save()
    {

        if ($this->id) {

            $this->update();

        } else {

            if (!empty($this->errors)) {
                return false;
            }

            if (empty($this->filename) || empty($this->tmp_path)) {
                $this->errors[] = "the file was not available";
                return false;
            }

            // TODO: FIX THIS!!!
//            $target_path = SITE_ROOT . DS . 'images' . DS . $this->upload_directory . DS . $this->filename;

            $target_path = SITE_ROOT . DS . $this->filename;
//            $target_path = "images/".$this->filename;

            if (file_exists($target_path)) {
                $this->errors[] = "The file {$this->filename} already exists";
                return false;

            }
            if (move_uploaded_file($this->tmp_path, $target_path)) {

                if ($this->create()) {
                    unset($this->tmp_path);
                    return true;
                }
            } else {

                $this->errors[] = "the file directory probably does not have permission";
                return false;
            }
        }
    }

    /**
     * Return path of picture
     * @return string
     */
    public function picturePath() {
        return $this->upload_directory.DS.$this->filename;
    }

    /**
     * Delete Photo
     * @return bool
     */
    public function deletePhoto() {
        if ($this->delete()) {

            return unlink($this->picturePath()) ? true : false;
        } else {
            return false;
        }

    }

    /**
     * Data for picture sidebar
     * @param $photo_id
     */
    public static function displaySidebarData($photo_id) {
        $photo = Photo::getById($photo_id);

        $display = "<a class='thumbnail' href='../photo.php?id={$photo_id}' title='View Photo on Site Post' target='_blank'><img class='selected-sidebar-img img-responsive'  src='{$photo->picturePath()}'></a>";
        $display .= "<p>File name: {$photo->filename}</p>";
        $display .= "<p>Type: {$photo->type}</p>";
        $display .= "<p>Size: {$photo->size}</p>";

        echo $display;
    }
}