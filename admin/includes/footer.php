  </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- wysiwyg -->
    <script src="http://tinymce.cachefly.net/4.1/tinymce.min.js"></script>

    <script src="js/scripts.js"></script>
    <script src="js/dropzone.js"></script>

  <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

          var data = google.visualization.arrayToDataTable([
              ['Section', 'Count'],
              ['New Views',     <?php echo $session->count; ?>],
              ['Comments', <?php echo Comment::countAll(); ?>],
              ['Users',  <?php echo User::countAll(); ?>],
              ['Photos', <?php echo Photo::countAll(); ?>],
          ]);

          var options = {
              title: 'Gallery System Statistic',
              pieSlice: 'label',
              backgroundColor: 'transparent',
              is3D: 'true'
          };

          var chart = new google.visualization.PieChart(document.getElementById('piechart'));

          chart.draw(data, options);
      }
  </script>
</body>

</html>
