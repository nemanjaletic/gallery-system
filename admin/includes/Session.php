<?php
/**
 * Created by PhpStorm.
 * User: nemanjaletic
 * Date: 5.8.20.
 * Time: 14.52
 */

class Session
{
    public $signed_id = false;
    public $user_id;
    private $message;
    public $count;

    /**
     * Session constructor.
     */
    public function __construct()
    {
        session_start();
        $this->checkLogin();
        $this->checkMessage();
        $this->visitorCount();
    }

    /**
     * Check is user login
     */
    private function checkLogin() {
        if (isset($_SESSION['user_id'])) {
            $this->user_id = $_SESSION['user_id'];
            $this->signed_id = true;
        } else {
            unset($this->user_id);
            $this->signed_id = false;
        }
    }

    /**
     * Check is user signed in
     * @return bool
     */
    public function isSignedIn() {
        return $this->signed_id;
    }

    /**
     * Login function
     * @param $user
     */
    public function login($user)
    {
        if ($user) {

            $this->user_id = $_SESSION['user_id'] = $user->id;
            $this->signed_id = true;
        }
    }

    /**
     * Logout function
     */
    public function logout() {

            unset($_SESSION['user_id']);
            unset($this->user_id);
            $this->signed_id = false;
    }

    /**
     * @param string $msg
     * @return mixed
     */
    public function message($msg = "") {
        if (!empty($msg)) {
            $_SESSION['message'] = $msg;
        } else {
            return $this->message;
        }
    }


    public function checkMessage() {
        if (isset($_SESSION['message'])) {
            $this->message = $_SESSION['message'];
            unset($_SESSION['message']);
        } else {
            $this->message = "";
        }
    }

    public function visitorCount() {
        if (isset($_SESSION['count'])) {
            return $this->count = $_SESSION['count']++;
        } else {
            return $_SESSION['count'] = 1;
        }
    }

}

$session = new Session();
$message = $session->message();