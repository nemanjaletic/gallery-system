<?php

require_once("new_config.php");

class Database
{

    public $connection;

    /**
     * Database constructor.
     */
    function __construct()
    {
        $this->open_db_connection();
    }

    /**
     * Open database connection
     */

    public function open_db_connection() {

        $this->connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

        if ($this->connection->connect_errno) {
            die ("Database connection is failed." . $this->connection->connect_error);
        }
    }

    /**
     * Create db query
     * @param $sql
     * @return mixed
     */
    public function query($sql) {

        $result = $this->connection->query($sql);

        $this->confirm_query($result);

        return $result;
    }

    /**
     * Confirm requested query
     * @param $result
     */
    private function confirm_query($result) {

        if (!$result) {
            die ("Query Failed" . $this->connection->error);
        }
    }

    /**
     * Escape string because of mysql injection
     * @param $string
     * @return mixed
     */
    public function escape_string($string) {
        return $this->connection->real_escape_string($string);
    }

    /**
     * @return mixed
     */
    public function the_insert_id() {
        return mysqli_insert_id($this->connection);
    }
}

/**
 * Create database object
 */
$database = new Database();
