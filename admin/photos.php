<?php include("includes/header.php"); ?>
<?php if (!$session->isSignedIn()) : ?>
    <?php redirect("login.php"); ?>
<?php endif; ?>

<?php
$page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;

$items_per_page =  12;

$items_total_count = Photo::countAll();




//$photos = Photo::getAll();

$pagination = new Pagination($page, $items_per_page, $items_total_count);

$sql = "SELECT * FROM photos ORDER BY id DESC LIMIT {$items_per_page} OFFSET {$pagination->offset()} ";

$photos = Photo::findByQuery($sql);

?>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

        <!-- Navigation -->
        <?php include "includes/navigation.php" ?>

        <?php include "includes/sidebar.php" ?>
    </nav>
    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <p class="bg-info"><?php echo $message; ?></p>
                    <h1 class="page-header">
                        Photos
                        <small>All Photos</small>
                    </h1>

                    <div class="">
                        <a href="upload.php" class="btn btn-primary">Add New Photo</a>
                    </div>

                    <div class="col-md-12">
                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th>Photo</th>
                                <th>ID</th>
                                <th>File Name</th>
                                <th>Title</th>
                                <th>Size</th>
                                <th>Posted Date</th>
                                <th>Comments</th>
                                <th colspan="3" class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($photos as $photo ) : ?>
                            <tr>
                                <td><img class="img-responsive img-rounded" src="<?php echo $photo->picturePath(); ?>"></td>
                                <td><?php echo $photo->id; ?></td>
                                <td><?php echo $photo->filename; ?></td>
                                <td><?php echo $photo->title; ?></td>
                                <td><?php echo $photo->size; ?></td>
                                <td><?php echo $photo->date; ?></td>
                                <?php $comments = Comment::getComments($photo->id); ?>

                                <td><a href="comments_photo.php?id=<?php echo $photo->id; ?>"><?php echo count($comments); ?></a></td>
                                <td><a class="delete_link" href="delete_photo.php?id=<?php echo $photo->id ?>">Delete</a></td>
                                <td><a href="edit_photo.php?id=<?php echo $photo->id ?>">Edit</a></td>
                                <td><a href="../photo.php?id=<?php echo $photo->id ?>">View</a></td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <!--    Pagination-->
                        <div class="row">
                            <ul class="pager">
                                <?php if ($pagination->pageTotal() > 1) : ?>
                                    <?php if ($pagination->hasPrevious()) : ?>
                                        <li class="previous"><a href="photos.php?page=<?php echo $pagination->previous(); ?>">Previous Page</a></li>
                                    <?php endif; ?>

                                    <?php for ($i = 1; $i <= $pagination->pageTotal(); $i++) : ?>
                                        <?php if ($i == $pagination->current_page) : ?>
                                            <li class="active"><a class="active_link" href="photos.php?page=<?php echo $i; ?>"><?php echo $i ?></a></li>
                                        <?php else : ?>
                                            <li class=""><a href="photos.php?page=<?php echo $i; ?>"><?php echo $i ?></a></li>
                                        <?php endif; ?>
                                    <?php endfor; ?>

                                    <?php if ($pagination->hasNext()) : ?>
                                        <li class="next"><a href="photos.php?page=<?php echo $pagination->next(); ?>" class="">Next Page</a></li>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- /.row -->

        </div>

    </div>
    <!-- /#page-wrapper -->

<?php include("includes/footer.php"); ?>