<?php include("includes/header.php"); ?>
<?php if (!$session->isSignedIn()) : ?>
    <?php redirect("login.php"); ?>
<?php endif; ?>

<?php
$message = "";
if (isset($_POST['create'])) {
    $user = new User();
    if ($user) {
       /* if (($_POST['password'] != $_POST['password_r'])) {
            $message = "Password or Repeat Password aren't correct.";
        } else {*/
            $user->first_name = $_POST['first_name'];
            $user->last_name = $_POST['last_name'];
            $user->username = $_POST['username'];
            $user->email = $_POST['email'];
            $user->password = $_POST['password'];

            $user->setFile($_FILES['user_image']);
            $user->uploadPhoto();
            $user->save();
            redirect("users.php");
            $session->message("User <i>" . $_POST['username'] . "</i> Saved! <a href='edit_user.php?id={$user->id}'>View</a> User");


//            if ($user->saveUser()) {
//                $message = "User Created Successfully. <a href='users.php'>View All Users.</a>";
//            } else {
//                $message = join("<br>", $user->errors);
//            }

        }


    //}
}



?>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

        <!-- Navigation -->
        <?php include "includes/navigation.php" ?>

        <?php include "includes/sidebar.php" ?>
    </nav>
    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Add New User
                        <small>Subheading</small>
                    </h1>

                    <form action="" method="post" enctype="multipart/form-data">
                        <p class="bg-info"> <?php echo $message; ?></p>
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <label for="">First Name</label>
                                <input type="text" class="form-control" name="first_name" required>
                            </div>
                            <div class="form-group">
                                <label for="">Last Name</label>
                                <input type="text" class="form-control" name="last_name" required>
                            </div>
                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="text" class="form-control" name="username" required>
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" class="form-control" name="email" required>
                            </div>
                            <div class="form-group">
                                <label for="">Password</label>
                                <input type="password" class="form-control" name="password" required>
                            </div>
                            <div class="form-group">
                                <label for="">Repeat Password</label>
                                <input type="password" class="form-control" name="password_r" required>
                            </div>
                            <div class="form-group">

                                <input type="file" name="user_image">

                            </div>
                        <div class="info-box-update pull-right ">
                            <input type="submit" name="create" value="Add User" class="btn btn-primary btn-lg ">
                        </div>

                        </div>

                    </form>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /#page-wrapper -->

<?php include("includes/footer.php"); ?>