<?php include("includes/header.php"); ?>
<?php if (!$session->isSignedIn()) : ?>
    <?php redirect("login.php"); ?>
<?php endif; ?>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

        <!-- Navigation -->
        <?php include "includes/navigation.php" ?>

        <?php include "includes/sidebar.php" ?>
    </nav>
        <div id="page-wrapper">

            <?php include("includes/admin_content.php"); ?>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>