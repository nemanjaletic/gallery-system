<?php  require_once "includes/header.php"; ?>

<?php
    if ($session->isSignedIn()) {
        redirect("index.php");
    }

    if (isset($_POST['submit'])) {
        $username = trim($_POST['username']);
        $password = trim($_POST['password']);

        // Method for check database
        $user_found = User::verifyUser($username, $password);

        if ($user_found) {
            $session->login($user_found);
            redirect("index.php");
        } else {
            $session->message("Your username or password are incorrect.");
        }
    } else { // If isn't set, because of "Notice"
        $username = "";
        $password = "";
        $message = "";
    }

?>

<div class="col-md-4 col-md-offset-3">

<!--    <h4 class="">LOGIN</h4>-->

    <form id="login-form" action="" method="post">
        <h4 class="bg-info login-msg"><?php echo $message; ?></h4>
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" name="username" value="<?php echo htmlentities($username); ?>" >

        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" value="<?php echo htmlentities($password); ?>">

        </div>


        <div class="form-group">
            <input type="submit" name="submit" value="Submit" class="btn btn-primary">

        </div>


    </form>


</div>