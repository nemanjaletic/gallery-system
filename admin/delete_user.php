<?php include "includes/init.php" ?>

<?php if (!$session->isSignedIn()) : ?>
    <?php redirect("login.php"); ?>
<?php endif; ?>

<?php
if (empty($_GET['id'])) {
    redirect("users.php");
}

$user = User::getById($_GET['id']);

if ($user) {

    $user->deleteUser();
    redirect("users.php");
    $session->message("The User has been Deleted!");

} else {
    redirect("users.php");

}
?>
