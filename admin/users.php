<?php include("includes/header.php"); ?>
<?php if (!$session->isSignedIn()) : ?>
    <?php redirect("login.php"); ?>
<?php endif; ?>

<?php
$page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;

$items_per_page =  12;

$items_total_count = Photo::countAll();




//$photos = Photo::getAll();

$pagination = new Pagination($page, $items_per_page, $items_total_count);

$sql = "SELECT * FROM users ORDER BY id DESC LIMIT {$items_per_page} OFFSET {$pagination->offset()}";

$users = User::findByQuery($sql);


?>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

        <!-- Navigation -->
        <?php include "includes/navigation.php" ?>

        <?php include "includes/sidebar.php" ?>
    </nav>
    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <p class="bg-info"><?php echo $message; ?></p>
                    <h1 class="page-header">
                        Users
                        <small>All Users</small>
                    </h1>

                    <div class="">
                        <a href="add_user.php" class="btn btn-primary">Add New User</a>
                    </div>

                    <div class="col-md-12">
                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Photo</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th colspan="3" class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($users as $user) : ?>
                                <tr>
                                    <td><?php echo $user->id; ?></td>
                                    <td><a href="" class="thumbnail"><img src="<?php echo  $user->userImage() ?>"></a></td>
                                    <td><?php echo $user->first_name; ?></td>
                                    <td><?php echo $user->last_name; ?></td>
                                    <td><?php echo $user->username; ?></td>
                                    <td><?php echo $user->email; ?></td>
                                    <td><a class="delete_link" href="delete_user.php?id=<?php echo $user->id ?>">Delete</a></td>
                                    <td><a href="edit_user.php?id=<?php echo $user->id ?>">Edit</a></td>
                                    <td><a href="view_user.php?id=<?php echo $user->id ?>">View</a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!-- /.row -->

        </div>

    </div>
    <!-- /#page-wrapper -->

<?php include("includes/footer.php"); ?>