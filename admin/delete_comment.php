<?php include "includes/init.php" ?>

<?php if (!$session->isSignedIn()) : ?>
    <?php redirect("login.php"); ?>
<?php endif; ?>

<?php
if (empty($_GET['id'])) {
    redirect("comments.php");
}

$comment = Comment::getById($_GET['id']);

if ($comment) {

    $comment->delete();
    redirect("comments.php");
    $session->message("The Comment has been Deleted!");
} else {
    redirect("comments.php");

}
?>
