<?php
//echo "<pre>";
//print_r($_FILES['file_upload']);
//echo "</pre>";

if (isset($_POST['submit'])) {

    $file_name= $_FILES['file_upload']['name'];
    $file_temp_name = $_FILES['file_upload']['tmp_name'];
    $directory = "uploads";

    $upload_errors = array(

        UPLOAD_ERR_OK => "There is no error.",
        UPLOAD_ERR_INI_SIZE => "The uploaded file exceeds the upload_max_filesize directive in php.ini.",
        UPLOAD_ERR_FORM_SIZE => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.",
        UPLOAD_ERR_PARTIAL => "The uploaded file was only partially uploaded.",
        UPLOAD_ERR_NO_FILE => "No file was uploaded.",
        UPLOAD_ERR_NO_TMP_DIR => "Missing a temporary folder.",
        UPLOAD_ERR_EXTENSION => "A PHP extension stoppred the file upload."

    );

    if (move_uploaded_file($file_temp_name, $directory . "/" . $file_name)) {
        $error_message = 'File Uploaded Successfully';
    } else {
        $error = $_FILES['file_upload']['error'];

        $error_message = $upload_errors[$error];
    }






}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
    </head>
    <body>
        <form action="upload.php" class="form-group" enctype="multipart/form-data" method="post">
            <h2 class="bg-warning">
                <?php if (!empty($upload_errors)) : ?>
                    <?php echo $error_message; ?>
                <?php endif; ?>
            </h2>

            <input type="file" name="file_upload"><br>
            <input type="submit" name="submit">
        </form>
    </body>
</html>